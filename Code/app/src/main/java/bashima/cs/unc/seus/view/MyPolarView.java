package bashima.cs.unc.seus.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by bashimaislam on 9/8/16.
 */


public class MyPolarView extends View {

    int width, height, delta;
    int plotWidth;

    void init()
    {
        width = 1000;
        height = 500;
        delta = 50;
        plotWidth = width - delta * 4;
    }

    public MyPolarView(Context context) {
        super(context);
        init();
    }

    public MyPolarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyPolarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public MyPolarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setColor(Color.LTGRAY);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);

        int r = height/2-20;
        for(; r >= 0; r = r - delta) {
            canvas.drawCircle(width / 2, height / 2, r, paint);
        }

        paint.setColor(Color.rgb(0, 0, 80));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(400, 250, 15, paint);

        paint.setColor(Color.DKGRAY);
        canvas.drawLine(width/5, height/2, 4*width/5, height/2, paint);
        canvas.drawLine(width/2, 0, width/2, height, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = width;
        int desiredHeight = height;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

}
