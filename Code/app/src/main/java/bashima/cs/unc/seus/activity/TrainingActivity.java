package bashima.cs.unc.seus.activity;

import android.content.Context;
import android.location.Address;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DefaultDataset;
import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.core.Instance;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import bashima.cs.unc.seus.constant.Constant;
import bashima.cs.unc.seus.featuer.MFCCFeatureExtract;
import bashima.cs.unc.seus.featuer.WindowFeature;
import libsvm.LibSVM;
import seus.bashima.cs.unc.seus.R;

public class TrainingActivity extends AppCompatActivity {

    Button btTrain;
    Button btFeature;
    public byte audio[];
    Dataset data;
    Context context;
    public LibSVM libSVM;
    String modelName = "model.ser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        context = this;
        btTrain = (Button) findViewById(R.id.bt_train);
        btTrain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                readMFCC();
            }
        });
        btFeature = (Button) findViewById(R.id.bt_calculate_feature);
        btFeature.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mfccCalcutation();
                Toast.makeText(context,"MFCC Calculated",Toast.LENGTH_LONG).show();
            }
        });
        data = new DefaultDataset();
        libSVM = new LibSVM();

    }

    public void mfccCalcutation() {

        File folder = new File(Constant.FILE_PATH, Constant.FOLDER_NAME);
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
            } else {
                String fileName = fileEntry.getName();
                String extention = "";
                int pos = fileName.lastIndexOf(".");
                if (pos > 0) {
                    extention = fileName.substring(pos + 1);
                    fileName = fileName.substring(0, pos);

                }
                if (extention.equals("wav")) {
                    File newFile = new File(fileName + "_1.txt");
                    Log.d("mfcc exists???",fileName + "_1.txt");
                    if (newFile.exists()) {
                        Log.d("nofile","NO FILE");
                    } else {
                        calculateMFCC(fileEntry.getName());
                    }
                }
            }
        }
    }


    public void calculateMFCC(String wavFileName) {
        File fileIn = new File(Constant.FILE_PATH + Constant.FOLDER_NAME, wavFileName);
        Log.i("current mfcc wav file", wavFileName);
        int fileSize = (int) fileIn.length();
        String[] nameWOExtension = wavFileName.split("\\.");
        Log.i("no",nameWOExtension.length+"");
        Log.i("ne",nameWOExtension[1]);

        audio = new byte[(int) fileIn.length()];//size & length of the file
        InputStream inputStream = null;
        int subArrayNumber = (int) Math.floor(fileSize / Constant.BUFFERSIZE);
        if (subArrayNumber < 1)
            return;


        try {
            inputStream = new FileInputStream(fileIn);

            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 44100);
            DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);      //  Create a DataInputStream to read the audio data from the saved file

            int i = 0;   //  Read the file into the "audio" array
            while (dataInputStream.available() > 0)

            {
                audio[i] = dataInputStream.readByte();     //  This assignment does not reverse the order
                i++;
            }

            dataInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < subArrayNumber; i++) {
//        for (int i = 0; i < 1; i++) {
            MFCCFeatureExtract mfccFeatures = null;
            List<WindowFeature> windowFeatureList = null;
            byte[] temp = new byte[Constant.BUFFERSIZE];
            System.arraycopy(audio, i * Constant.BUFFERSIZE, temp, 0, Constant.BUFFERSIZE);
            double[] inputSignal = Constant.convertSignalToDouble(temp);
//            Log.d("imput signal length", inputSignal.length + "");
            mfccFeatures = new MFCCFeatureExtract(inputSignal, Constant.Tw, Constant.Ts, Constant.SAMPLE_RATE, Constant.Wl);
            windowFeatureList = mfccFeatures.getListOfWindowFeature();
            double [] feature = MFCCFeatureExtract.generateDataSet(windowFeatureList);

            Log.d("before write", doubleToString(feature));
            try {
//                Log.v("full name", Constant.FILE_PATH + Constant.FOLDER_NAME + nameWOExtension[1] + "_" + (i + 1) + ".ser");
//                Log.d("only name", nameWOExtension[0] + "_" + (i + 1) + ".ser");
                FileOutputStream fout = new FileOutputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + nameWOExtension[0] + "_" + (i + 1) + ".txt");
                ObjectOutputStream oos = new ObjectOutputStream(fout);
                oos.writeObject(feature);
                oos.close();
                System.out.println("Done");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        Log.v("finish","mfcc calculated");
    }

    public void readMFCC()
    {

        File folder = new File(Constant.FILE_PATH, Constant.FOLDER_NAME);
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
            } else {
                String fileName = fileEntry.getName();
                String extention = "";
                int pos = fileName.lastIndexOf(".");
                if (pos > 0) {
                    extention = fileName.substring(pos + 1);
                }
                if (extention.equals("txt")) {
                    try{

                        FileInputStream fin = new FileInputStream(fileEntry);
                        ObjectInputStream ois = new ObjectInputStream(fin);
                        double[] feature = (double[]) ois.readObject();
                        ois.close();
                        String className ="";
                        if(fileName.contains("car"))
                        {
                            className =  "positive";
                        }
                        else
                        {
                            className = "negative";
                        }
                        Instance instance = new DenseInstance(feature,className);
                        data.add(instance);
                        Log.d("after read",doubleToString(feature));

                    }catch(Exception ex){
                        ex.printStackTrace();

                    }
                }
            }
        }
        libSVM.buildClassifier(data);
        FileOutputStream fout = null;
        File f = new File(getCacheDir()+modelName);
//        try {
//
//            InputStream is = getAssets().open(detectionModelName);
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//
//
//            FileOutputStream fos = new FileOutputStream(f);
//            fos.write(buffer);
//            fos.close();
//        } catch (Exception e) { throw new RuntimeException(e); }
        try {
            Log.d("classifier","classifier built");
            fout = new FileOutputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + modelName);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(libSVM);
            oos.close();
            Log.d("classifier","classifier built done");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(context,"Classifier Build", Toast.LENGTH_LONG).show();


    }

    public String doubleToString(double[] doub){
        String temp ="";
        for(int i=0; i<doub.length; i++)
        {
            temp = temp + " "+ doub[i];
        }
        return temp;
    }
}
