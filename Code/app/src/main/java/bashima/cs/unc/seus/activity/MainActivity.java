package bashima.cs.unc.seus.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import net.sf.javaml.classification.Classifier;
import net.sf.javaml.core.DenseInstance;
import net.sf.javaml.core.Instance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import bashima.cs.unc.seus.Object.BTData;
import bashima.cs.unc.seus.Service.UartService;
import bashima.cs.unc.seus.constant.Constant;
import bashima.cs.unc.seus.featuer.MFCCFeatureExtract;
import bashima.cs.unc.seus.featuer.WindowFeature;
import bashima.cs.unc.seus.view.MyPlotView;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import libsvm.LibSVM;
import seus.bashima.cs.unc.seus.R;

import static bashima.cs.unc.seus.constant.Constant.DELIM;
import static bashima.cs.unc.seus.constant.Constant.REQUEST_SELECT_DEVICE;
import static bashima.cs.unc.seus.constant.Constant.UART_PROFILE_CONNECTED;
import static bashima.cs.unc.seus.constant.Constant.UART_PROFILE_DISCONNECTED;

public class MainActivity extends AppCompatActivity {
    // Decalring Variables
    // Detection
    String deviceName;
    boolean isRunning = false;
    ToggleButton btStart = null;
    public byte audioData[] = null;
    private int audioBufferSize;
    private AudioRecord recorder;
    private Thread readAudioThread;
    BlockingQueue<byte[]> audioSharedQueue = null;
    LibSVM libsvm;
    int detected = Constant.NONE;

    // UI
    MyPlotView myPlotView = null;
    private double audioPlotValue = 0;
    TextView tvDeviceName;
    CheckBox cbSound;
    CheckBox cbVibrate;
    private PieChartView chart;
    private PieChartData data;
    SliceValue sliceValue1, sliceValue2;

    // Car Detection and Localization
    private Thread carDetecLocal = null;

    //Localization
    public boolean btState = false;
    private BluetoothAdapter mBtAdapter = null;
    private Button btnConnectDisconnect;
    private UartService mService = null;
    private BluetoothDevice mDevice = null;
    public int mState = UART_PROFILE_DISCONNECTED;
    public ArrayList<BTData> btDataArrayList = new ArrayList<>();
    Classifier classifierCarDirection;
    Classifier classifierCarDistance;
    Classifier classifierHornDirection;
    Classifier classifierHornDistance;

    public int counter = 0;
    private static boolean delim_found = false;
    private static int delim_index = 0;
    // Keep track of bytes found in data reconstruction
    private static byte[] byte_data = new byte[DELIM[DELIM.length - 1]];
    // Keep track of where we are in data reconstruction
    private static int data_index = 0;
    private static boolean printed = false;


    //application
    public Context context = null;
    public MainActivity mainActivity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;
        context = this;

        //UI
        myPlotView = (MyPlotView) findViewById(R.id.plotviewid);
        tvDeviceName = (TextView) findViewById(R.id.tv_connected_device);
        cbSound = (CheckBox) findViewById(R.id.cb_sound);
        cbVibrate = (CheckBox) findViewById(R.id.cb_vibrate);
        chart = (PieChartView) findViewById(R.id.chart);
        chart.setChartRotationEnabled(false);

        sliceValue1 = new SliceValue((float)  15, getResources().getColor(R.color.primary));
        sliceValue2 = new SliceValue((float)  15, getResources().getColor(R.color.divider));



        generatePi(0);

        Log.d("ReadModel", "Start");
        readModel();
        Log.d("ReadModel", "End");

        //setup audio buffer
        try {
            audioBufferSize = AudioRecord
                    .getMinBufferSize(Constant.SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                            AudioFormat.ENCODING_PCM_16BIT);
            //recalculate audioBufferSize
            int n = (int) Math.ceil(Constant.DETECTION_WINDOW_SIZE / audioBufferSize);
            audioBufferSize = n * audioBufferSize;
            Constant.BUFFERSIZE = audioBufferSize;
            audioData = new byte[Constant.BUFFERSIZE];
        } catch (Exception e) {
            android.util.Log.e("TrackingFlow", "Exception", e);
        }


        //Bluetooth
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBtAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        btnConnectDisconnect = (Button) findViewById(R.id.btn_select);

        service_init();
        // Handle Disconnect & Connect button
        btnConnectDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBtAdapter.isEnabled()) {
                    Log.i(Constant.TAG, "onClick - BT not enabled yet");
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, Constant.REQUEST_ENABLE_BT);
                } else {
                    if (btnConnectDisconnect.getText().equals("Connect")) {

                        //Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices

                        Intent newIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                        startActivityForResult(newIntent, REQUEST_SELECT_DEVICE);
//                        service_init();
                    } else {
                        //Disconnect button pressed
                        if (mDevice != null) {
                            mService.disconnect();
                        }
                    }
                }
            }
        });

        startButtonListener();

    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d(Constant.TAG, "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e(Constant.TAG, "Unable to initialize Bluetooth");
                finish();
            }

        }


        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };

    private Handler mHandler = new Handler() {
        @Override

        //Handler events that received from UART service
        public void handleMessage(Message msg) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_SELECT_DEVICE:
                //When the DeviceListActivity return, with the selected device address
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
                    mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(deviceAddress);

                    Log.d(Constant.TAG, "... onActivityResultdevice.address==" + mDevice + "mserviceValue" + mService);
                    deviceName = mDevice.getName();
                    tvDeviceName.setText(mDevice.getName()+ " - connecting");
                    mService.connect(deviceAddress);
                }
                break;
            case Constant.REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(this, "Bluetooth has turned on ", Toast.LENGTH_SHORT).show();

                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(Constant.TAG, "BT not enabled");
                    Toast.makeText(this, "Problem in BT Turning ON ", Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            default:
                Log.e(Constant.TAG, "wrong request code");
                break;
        }
    }

    public void readModel()
    {
//        ReadingModel
        if (Constant.isColumbia) {
            File detectModelFile = new File(getCacheDir() + Constant.detectionModelName);
            try {
                InputStream detecModelReadInputStream = getAssets().open(Constant.detectionModelName);
                int detectModelReadBufferSize = detecModelReadInputStream.available();
                byte[] detectModelReadBuffer = new byte[detectModelReadBufferSize];
                detecModelReadInputStream.read(detectModelReadBuffer);
                detecModelReadInputStream.close();

                FileOutputStream detectModelFileOutputStream = new FileOutputStream(detectModelFile);
                detectModelFileOutputStream.write(detectModelReadBuffer);
                detectModelFileOutputStream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            File carDistModelFile = new File(getCacheDir() + Constant.carDistModelName);
            try {
                InputStream carDistModelReadInputStream = getAssets().open(Constant.carDistModelName);
                int carDistModelReadBufferSize = carDistModelReadInputStream.available();
                byte[] carDistModelReadBuffer = new byte[carDistModelReadBufferSize];
                carDistModelReadInputStream.read(carDistModelReadBuffer);
                carDistModelReadInputStream.close();

                FileOutputStream carDistModelFileOutputStream = new FileOutputStream(carDistModelFile);
                carDistModelFileOutputStream.write(carDistModelReadBuffer);
                carDistModelFileOutputStream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            File hornDistModelFile = new File(getCacheDir() + Constant.hornDistModelName);
            try {
                InputStream hornDistModelReadInputStream = getAssets().open(Constant.hornDistModelName);
                int hornDistModelReadBufferSize = hornDistModelReadInputStream.available();
                byte[] hornDistModelReadBuffer = new byte[hornDistModelReadBufferSize];
                hornDistModelReadInputStream.read(hornDistModelReadBuffer);
                hornDistModelReadInputStream.close();

                FileOutputStream hornDistModelFileOutputStream = new FileOutputStream(hornDistModelFile);
                hornDistModelFileOutputStream.write(hornDistModelReadBuffer);
                hornDistModelFileOutputStream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            File hornDirModelFile = new File(getCacheDir() + Constant.hornDirModelName);
            try {
                InputStream hornDirModelReadInputStream = getAssets().open(Constant.hornDirModelName);
                int hornDirModelReadBufferSize = hornDirModelReadInputStream.available();
                byte[] hornDirModelReadBuffer = new byte[hornDirModelReadBufferSize];
                hornDirModelReadInputStream.read(hornDirModelReadBuffer);
                hornDirModelReadInputStream.close();

                FileOutputStream hornDirModelFileOutputStream = new FileOutputStream(hornDirModelFile);
                hornDirModelFileOutputStream.write(hornDirModelReadBuffer);
                hornDirModelFileOutputStream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            File carDirModelFile = new File(getCacheDir() + Constant.carDirModelName);
            try {
                InputStream carDirModelReadInputStream = getAssets().open(Constant.carDirModelName);
                int carDirModelReadBufferSize = carDirModelReadInputStream.available();
                byte[] carDirModelReadBuffer = new byte[carDirModelReadBufferSize];
                carDirModelReadInputStream.read(carDirModelReadBuffer);
                carDirModelReadInputStream.close();

                FileOutputStream carDirModelFileOutputStream = new FileOutputStream(carDirModelFile);
                carDirModelFileOutputStream.write(carDirModelReadBuffer);
                carDirModelFileOutputStream.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        try {
            FileInputStream detectFileInputStream, carDistFileInputStream, carDirFileInputStream, hornDistFileInputStream, hornDirFileInputStream;
            if (Constant.isColumbia) {
                detectFileInputStream = new FileInputStream(getCacheDir() + Constant.detectionModelName);
                carDistFileInputStream = new FileInputStream(getCacheDir() + Constant.carDistModelName);
                carDirFileInputStream = new FileInputStream(getCacheDir() + Constant.carDirModelName);
                hornDistFileInputStream = new FileInputStream(getCacheDir() + Constant.hornDistModelName);
                hornDirFileInputStream = new FileInputStream(getCacheDir() + Constant.hornDirModelName);
            } else {
                detectFileInputStream = new FileInputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + Constant.detectionModelName);
                carDistFileInputStream = new FileInputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + Constant.carDistModelName);
                carDirFileInputStream = new FileInputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + Constant.carDirModelName);
                hornDistFileInputStream = new FileInputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + Constant.hornDistModelName);
                hornDirFileInputStream = new FileInputStream(Constant.FILE_PATH + Constant.FOLDER_NAME + Constant.hornDirModelName);
            }
            ObjectInputStream ois = new ObjectInputStream(detectFileInputStream);
            libsvm = (LibSVM) ois.readObject();
            ois.close();
            ObjectInputStream cDistOis = new ObjectInputStream(carDistFileInputStream);
            classifierCarDistance = (Classifier) cDistOis.readObject();
            cDistOis.close();
            ObjectInputStream cDirOis = new ObjectInputStream(carDirFileInputStream);
            classifierCarDirection = (Classifier) cDirOis.readObject();
            cDirOis.close();
            ObjectInputStream hDistOis = new ObjectInputStream(hornDistFileInputStream);
            classifierHornDistance = (Classifier) hDistOis.readObject();
            hDistOis.close();
            ObjectInputStream hDirOis = new ObjectInputStream(hornDirFileInputStream);
            classifierCarDirection = (Classifier) hDirOis.readObject();
            hDirOis.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.train:
                Intent trainIntent = new Intent(mainActivity, TrainingActivity.class);
                mainActivity.startActivity(trainIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void generatePi(int segment){
        List<SliceValue> values = new ArrayList<SliceValue>();
        for(int i=0; i<8; i++)
        {
            if(i==segment-1)
            {
                values.add(sliceValue1);
            }
            else
            {
                values.add(sliceValue2);
            }
        }
        data = new PieChartData(values);
        chart.setPieChartData(data);
    }

    void startButtonListener() {
        btStart = (ToggleButton) findViewById(R.id.toggleButton);
        btStart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isRunning = true;
                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, Constant.SAMPLE_RATE,
                            AudioFormat.CHANNEL_IN_DEFAULT,
                            AudioFormat.ENCODING_PCM_16BIT, audioBufferSize * 2);
                    audioSharedQueue = new LinkedBlockingQueue<>();
                    recorder.startRecording();
                    readAudioThread = new Thread(new Runnable() {
                        public void run() {
                            while (readAudioThread != null && !readAudioThread.isInterrupted()) {
                                readAudioBuffer();
                                try {
                                    readAudioThread.sleep(300);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        updatePlotView(audioPlotValue);
                                    }
                                });
                            }
                        }
                    });
                    readAudioThread.start();
                    carDetecLocal = new Thread(new CarDetectionLocalization());
                    carDetecLocal.start();
                    String message = "SEUSS";
                    byte[] value;
                    try {
                        //send data to service
                        value = message.getBytes("UTF-8");
                        mService.writeRXCharacteristic(value);
                        //Update the log with time stamp
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                } else {
                    isRunning = false;
                    readAudioThread.interrupt();
                    readAudioThread = null;
                    carDetecLocal.interrupt();
                    try {
                        if (recorder != null) {
                            recorder.stop();
                            recorder.release();
                            recorder = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String message = "SEUSE";
                    byte[] value;
                    try {
                        //send data to service
                        value = message.getBytes("UTF-8");
                        mService.writeRXCharacteristic(value);
                        //Update the log with time stamp
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void readAudioBuffer() {
        int bufferReadResult;
        try {
            if (recorder != null) {
                while (isRunning) {
                    bufferReadResult = recorder.read(audioData, 0, audioBufferSize);
                    if (bufferReadResult != AudioRecord.ERROR_INVALID_OPERATION) {
                        audioSharedQueue.put(audioData);
                        double sumLevel = 0;
                        for (int i = 0; i < bufferReadResult; i++) {
                            sumLevel += audioData[i];
                        }
                        audioPlotValue = Math.abs((sumLevel / bufferReadResult));
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                updatePlotView(audioPlotValue);
                            }
                        });
                    }
                }
                recorder.stop();
                recorder.release();
                recorder = null;
                readAudioThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class CarDetectionLocalization implements Runnable {
        MFCCFeatureExtract mfccFeatures;
        List<WindowFeature> windowFeaturelst;

        CarDetectionLocalization() {
            mfccFeatures = null;
            windowFeaturelst = null;
        }

        @Override
        public void run() {
            int carPredict = 0;
            while (isRunning) {
                try {
                    //audio feature extract
                    byte[] windowSizeSamples = audioSharedQueue.take();
                    if (windowSizeSamples.length != 0) {
                        double[] inputSignal = Constant.convertSignalToDouble(windowSizeSamples);
                        mfccFeatures = new MFCCFeatureExtract(inputSignal, Constant.Tw, Constant.Ts, Constant.SAMPLE_RATE, Constant.Wl);
                        windowFeaturelst = mfccFeatures.getListOfWindowFeature();
                        double[] feature = MFCCFeatureExtract.generateDataSet(windowFeaturelst);
                        //classification instance create
                        Instance instance = new DenseInstance(feature);
                        //classify detection
                        Object predictedClassValue = libsvm.classify(instance);
                        if (predictedClassValue.equals("positive")) {
                            detected = Constant.CAR;
                            carPredict++;
                            if (carPredict > 3) {
                                Alert();
                            }

                            double[] btFeature = btDataArrayList.get(btDataArrayList.size() - 1).FeatureGen();
                            Instance localInstance = new DenseInstance(btFeature);
                            Object carDistVal = classifierCarDistance.classify(localInstance);
                            Object carDirVal = classifierCarDirection.classify(localInstance);
                        } else if (predictedClassValue.equals("hpositive")) {
                            detected = Constant.HORN;
                            carPredict++;
                            Alert();

                            double[] btFeature = btDataArrayList.get(btDataArrayList.size() - 1).FeatureGen();
                            Instance localInstance = new DenseInstance(btFeature);
                            Object hornDistVal = classifierHornDistance.classify(localInstance);
                            Object hornDirVal = classifierHornDirection.classify(localInstance);

                        } else {
                            carPredict = 0;
                            detected = Constant.NONE;

                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            carDetecLocal = null;
        }
    }

    //Bluetooth service initialization
    private void service_init() {
        Log.e("SERVICEINTIT", "serviceinit......................");

        Intent bindIntent = new Intent(this, UartService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(UARTStatusChangeReceiver, makeGattUpdateIntentFilter());
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }




    void updatePlotView(double val) {
        myPlotView.insertPoint(val, detected);
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            public void run() {
                myPlotView.invalidate();
            }
        });
    }

    public final BroadcastReceiver UARTStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(UartService.ACTION_GATT_CONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Log.d(Constant.TAG, "UART_CONNECT_MSG");
                        btnConnectDisconnect.setText(R.string.disconnect);
                        deviceName = mDevice.getName();
                        tvDeviceName.setText(mDevice.getName() + R.string.connected);
                        mState = UART_PROFILE_CONNECTED;
//                        fopen();
                        counter = 0;
                    }

                });
            }
            if (action.equals(UartService.ACTION_GATT_DISCONNECTED)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        btnConnectDisconnect.setText(R.string.connect);
                        deviceName = "";
                        tvDeviceName.setText(R.string.none_connect);
                        mState = UART_PROFILE_DISCONNECTED;
                        mService.close();
//                        file_close();

                        // Deinitialize everything
                        delim_found = false;
                        delim_index = 0;
                        data_index = 0;
                        counter = 0;
                    }
                });
            }

            if (action.equals(UartService.ACTION_GATT_SERVICES_DISCOVERED)) {
                mService.enableTXNotification();
            }

            if (action.equals(UartService.ACTION_DATA_AVAILABLE)) {
                final byte[] txValue = intent.getByteArrayExtra(UartService.EXTRA_DATA);
                Log.d("Recv bytes raw:", "" + txValue.length);
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            System.out.println(txValue.length);
                            for (byte aTxValue : txValue) {
                                BTData data = new BTData();

                                // Case where delimiter is already found; parse data
                                if (delim_found) {

                                    // Keep adding bytes to the byte array until we have read all windows.
                                    byte_data[data_index++] = aTxValue;

                                    // If all bytes have been read, then save them to file
                                    if (data_index >= byte_data.length) {
                                        int b1, b2, b3, b4;

                                        // Write delays
                                        data.ccr[0] = byte_data[0];
                                        data.ccr[0] = byte_data[1];
                                        data.ccr[0] = byte_data[2];

                                        // Write powers; skip first four MSB bytes since
                                        // We don't need that much precision
                                        b1 = byte_data[7] & 0xFF;
                                        b2 = byte_data[8] & 0xFF;
                                        b3 = byte_data[9] & 0xFF;
                                        b4 = byte_data[10] & 0xFF;
                                        data.pwr[0] = ((b1 << 24) | (b2 << 16) | (b3 << 8) | b4);

                                        b1 = byte_data[15] & 0xFF;
                                        b2 = byte_data[16] & 0xFF;
                                        b3 = byte_data[17] & 0xFF;
                                        b4 = byte_data[18] & 0xFF;
                                        data.pwr[1] = ((b1 << 24) | (b2 << 16) | (b3 << 8) | b4);

                                        b1 = byte_data[23] & 0xFF;
                                        b2 = byte_data[24] & 0xFF;
                                        b3 = byte_data[25] & 0xFF;
                                        b4 = byte_data[26] & 0xFF;
                                        data.pwr[2] = ((b1 << 24) | (b2 << 16) | (b3 << 8) | b4);

                                        // Write out ZCR
                                        b1 = byte_data[27] & 0xFF;
                                        b2 = byte_data[28] & 0xFF;
                                        data.zcr[0] = ((b1 << 8) | b2);

                                        b1 = byte_data[29] & 0xFF;
                                        b2 = byte_data[30] & 0xFF;
                                        data.zcr[1] = ((b1 << 8) | b2);

                                        b1 = byte_data[31] & 0xFF;
                                        b2 = byte_data[32] & 0xFF;
                                        data.zcr[2] = ((b1 << 8) | b2);

                                        b1 = byte_data[33] & 0xFF;
                                        b2 = byte_data[34] & 0xFF;
                                        data.zcr[3] = ((b1 << 8) | b2);
                                        data.FeatureGen();
                                        Log.d("BLUETOOTH", data.toString());

                                        btDataArrayList.add(data);
                                        counter++;
                                        if (btDataArrayList.size() > 20) {
                                            btDataArrayList.remove(0);
                                        }
                                        // All bytes found, so need to find next window
                                        delim_found = false;
                                        delim_index = 0;
                                        data_index = 0;
                                    }
                                }

                                // Case where delimiter not found
                                else {

                                    // The byte is part of the delimiter we are looking for
                                    if (aTxValue == DELIM[delim_index] || delim_index == DELIM.length - 2) {

                                        // Just found the delimiter, so now we start reading data
                                        if (++delim_index >= DELIM.length) {
                                            delim_index = 0;
                                            delim_found = true;
                                        }
                                    }

                                    // Byte is not part of the delimiter we are looking for
                                    // Reset and start looking again
                                    else {
                                        delim_index = 0;
                                        delim_found = false;
                                    }
                                }
                            }

                            // Display packet count every second, which corresponds to 20 packets
                            // if sampling = 32 kHz, window size = 100 ms with 50 ms overlap
                            if (counter % 10 == 0 && !printed) {

                                tvDeviceName.setText(deviceName + ", " + counter);
                                printed = true;
                            } else {
                                printed = false;
                            }

                        } catch (Exception e) {
                            Log.e(Constant.TAG, e.toString());
                        }
                    }
                });
            }
            if (action.equals(UartService.DEVICE_DOES_NOT_SUPPORT_UART)) {
                Toast.makeText(context, "Device doesn't support UART. Disconnecting", Toast.LENGTH_LONG).show();
                mService.disconnect();
            }


        }
    };

    public void Alert() {
        if (cbSound.isChecked()) {
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cbVibrate.isChecked()) {

            Vibrator vibrator;
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
        }
    }


    @Override
    public void onBackPressed() {
        if (mState == UART_PROFILE_CONNECTED) {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
//            showMessage("nRFUART's running in background.\n             Disconnect to exit");
        }
        else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.popup_title)
                    .setMessage(R.string.popup_message)
                    .setPositiveButton(R.string.popup_yes, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.popup_no, null)
                    .show();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constant.TAG, "onDestroy()");

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(UARTStatusChangeReceiver);
        } catch (Exception ignore) {
            Log.e(Constant.TAG, ignore.toString());
        }
        unbindService(mServiceConnection);
        mService.stopSelf();
        mService= null;

    }

}
