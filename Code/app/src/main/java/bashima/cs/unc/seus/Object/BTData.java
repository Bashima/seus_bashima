package bashima.cs.unc.seus.Object;

/**
 * Created by bashimaislam on 10/21/16.
 */

public class BTData {
    public double []ccr = new double[3];
    public double []pwr = new double[3];
    public double []zcr = new double[4];
    public double []feature = new double[10];

    public double[] FeatureGen()
    {
        for (int i=0; i<3; i++)
        {
            feature[i] = ccr[i];
            feature[i+3] = pwr[i];
            feature[i+6] = zcr[i];
        }
        feature[9] = zcr[3];
        return feature;
    }

    public String toString(){
        String str = "";
        for (double i:feature
             ) {
            str = str + " " + i;
        }
        return str;
    }
}
