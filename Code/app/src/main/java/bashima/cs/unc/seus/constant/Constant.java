package bashima.cs.unc.seus.constant;

import android.os.Environment;

/**
 * Created by bashimaislam on 9/14/16.
 */
public class Constant {
    public static String detectionModelName = "model.ser";
    public static final int CAR = 1;
    public static final int NONE = 0;
    public static final int HORN = 2;
    public static final int SAMPLE_RATE = 44100;
    public static final int SAMPLE_DELAY = 0;
    public static final String FOLDER_NAME = "/SEUS/";
    public static final int DETECTION_WINDOW_SIZE = 40960;    //byte size (10 frames; 250 ms/frame; 2048 sample/frame)
    public static double Tw = 22;                // analysis frame duration (ms)
    public static double Ts = 22;                // analysis frame shift (ms)
    public static double Wl = 0.45;				   // window duration (second) //0.45
    public static String FILE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
    public static int BUFFERSIZE;
    public static final int REQUEST_SELECT_DEVICE = 1;
    public static final int REQUEST_ENABLE_BT = 2;
    public static final int UART_PROFILE_READY = 10;
    public static final String TAG = "nRFUART";
    public static final int UART_PROFILE_CONNECTED = 20;
    public static final int UART_PROFILE_DISCONNECTED = 21;
    public static boolean isColumbia = true;


    // Start message to write to BLE module to begin transmission
    public static final byte [] START_MSG = new byte[] {(byte)83, (byte)69, (byte)85, (byte)83, (byte)83};  //"SEUSS"

    // Delimiter between windows; final byte is the byte data length; Second to last byte is the microphone flag byte
    public static final int[] DELIM = {83, 69, 85, 83, 35};
    public static java.lang.String carDistModelName = "carDistModel.ser";
    public static java.lang.String hornDistModelName = "hornDistModel.ser";
    public static java.lang.String hornDirModelName = "hornDirModel.ser";
    public static java.lang.String carDirModelName = "carDirModel.ser";

    public static double[] convertSignalToDouble(byte[] byteData) {
        int bytePerSample = 2;
        int numSamples = byteData.length / bytePerSample;
        double[] amplitudes = new double[numSamples];

        int pointer = 0;
        for (int i = 0; i < numSamples; i++) {
            short amplitude = 0;
            for (int byteNumber = 0; byteNumber < bytePerSample; byteNumber++) {
                // little endian
                amplitude |= (short) ((byteData[pointer++] & 0xFF) << (byteNumber * 8));
            }
            amplitudes[i] = amplitude;
        }

        return amplitudes;
    }
}
